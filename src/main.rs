use std::io::Read;
use std::fs::File;

fn read_input_form_file(path: &String) -> String {
    let mut f = File::open(path).unwrap();
    let mut buffer = String::new();
    f.read_to_string(&mut buffer).unwrap();
    buffer
}

#[derive(Clone)]
enum ParseResult {
    Normal(Vec<String>),
    Finished(i32),
    Error(usize)
}


impl ParseResult {
    fn from_string(data: &String) -> ParseResult {
        let data: Vec<&str> = data.trim().split_whitespace().collect();
        match data.len() {
            0 | 2 => ParseResult::Error(1),
            1 => {
                ParseResult::Finished(data[0].parse::<i32>().unwrap())
            }
            _ => {
                let mut temp: Vec<String> = vec![];
                for item in data {
                    temp.push((*item).to_string());
                }
                ParseResult::Normal(temp)
            }
        }
    }
}


impl Iterator for ParseResult {
    type Item = (ParseResult, Option<i32>);

    fn next(&mut self) -> Option<Self::Item> {
        match *self {
            ParseResult::Error(v) => {
                return Some((self.clone(), Some(v as i32)));
            }
            ParseResult::Finished(v) => {
                return Some((self.clone(), Some(v)));
            }
            ParseResult::Normal(ref mut v) => {
                let x = v.remove(0).parse::<i32>().expect("input data error");
                let y = v.remove(0).parse::<i32>().expect("input data error");
                let sign = v.remove(0);
                let t = match &*sign {
                    "+" => {
                        x + y
                    }
                    "-" => {
                        x - y
                    }
                    "*" => {
                        x * y
                    }
                    "/" => {
                        x / y
                    }
                    _ => {
                        panic!("input data error");
                    }
                };
                v.insert(0, t.to_string());
                Some((ParseResult::from_string(&v.join(" ")), None))
            }
        }
    }
}

fn main() {
    let input = read_input_form_file(&"test_data/input.dat".to_string());
    let mut test = ParseResult::from_string(&input);
    match &test {
        &ParseResult::Normal(ref v) => println!("Starting state{:?}", v),
        &ParseResult::Finished(ref v) => {
            println!("already finished: {}", v);
            return;
        }
        &ParseResult::Error(ref err) => {
            println!("Error code: {}", err);
            panic!()
        }
    }
    loop {
        match test.next() {
            Some((ParseResult::Normal(v), None)) => {
                println!("current state: {:?}", v);
            }
            Some((ParseResult::Finished(_), Some(v))) => {
                println!("result: {}", v);
                break;
            }
            Some((ParseResult::Error(_), Some(err))) => {
                println!("terrible error: {}", err);
                break;
            }
            Some((ParseResult::Finished(val), None)) => {
                println!("result(none): {}", val);
                break;
            }
            _ => panic!("Input data error")
        }
    }
}
